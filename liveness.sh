#!/bin/bash
set -e

# Name of the ingress controller
INGRESS_CONTROLLER="$1"
if [ -z "$INGRESS_CONTROLLER" ]; then
    echo "Usage: liveness.sh INGRESS_CONTROLLER_NAME" 1>&2
    exit 1
fi

_oc() {
    oc --request-timeout=5s "$@"
}

# Figure out the type of ingress we have
RESOURCE_NAME=
RESOURCE_TYPE=
# See publish_dns.sh for an explanation
INGRESS_TYPE="$(_oc get ingresscontroller "${INGRESS_CONTROLLER}" -n openshift-ingress-operator -o jsonpath='{.spec.endpointPublishingStrategy.type}')"
if [[ "$INGRESS_TYPE" == "HostNetwork" ]]; then
    RESOURCE_TYPE="endpoints"
    RESOURCE_NAME="router-internal-${INGRESS_CONTROLLER}"
elif [[ "$INGRESS_TYPE" == "LoadBalancerService" ]]; then
    RESOURCE_TYPE="services"
    RESOURCE_NAME="router-${INGRESS_CONTROLLER}"
else
    echo "ERROR! Unsupported ingress type '${INGRESS_TYPE}'" 1>&2
    exit 1
fi

# Get the IP(s) of the ingress from Kubernetes API
IPV4_ADDRESSES=
if [[ "$INGRESS_TYPE" == "HostNetwork" ]]; then
    IPV4_ADDRESSES="$(_oc get "${RESOURCE_TYPE}/${RESOURCE_NAME}" -n openshift-ingress -o jsonpath='{.subsets[*].addresses[*].ip}' | tr ' ' '\n' | sort)"
elif [[ "$INGRESS_TYPE" == "LoadBalancerService" ]]; then
    IPV4_ADDRESSES="$(_oc get "${RESOURCE_TYPE}/${RESOURCE_NAME}" -n openshift-ingress -o jsonpath='{.status.loadBalancer.ingress[0].ip}')"
    if [ -z "$IPV4_ADDRESSES" ]; then
      LB_HOSTNAME="$(_oc get "${RESOURCE_TYPE}/${RESOURCE_NAME}" -n openshift-ingress -o jsonpath='{.status.loadBalancer.ingress[0].hostname}')"
      IPV4_ADDRESSES="$(dig +time=5 "${LB_HOSTNAME}" A +short)"
    fi
fi

# Get the currently active IP(s) from DNS
ROUTER_DOMAIN="$(_oc get ingresscontroller "${INGRESS_CONTROLLER}" -n openshift-ingress-operator -o jsonpath='{.status.domain}')"
DNS_IPS="$(dig +time=5 +tries=2 "$ROUTER_DOMAIN" A +short | sort)"

# Compare them
test "${IPV4_ADDRESSES}" == "${DNS_IPS}"
