# Publish DNS component

This project holds the script used by `custom-ingress-deployment` to publish DNS records
of ingress controllers. 
The project builds a docker image based on the provided `Dockerfile`.

`publish_dns.sh` script queries all `endpoint` objects in the `openshift-ingress` namespaces
and performs some actions based on each change. By default the script updates only the `internal`
view. The `external` view can be updated if only a specific argument is passed to the script.

Required arguments that `publish_dns.sh` needs to perform its actions:

* `INGRESS_CONTROLLER` - the name of the ingress controller;
* `DNS_MANAGER` - address of the DNS server that will be used to add/delete records;
* `ZONE` - the zone to be updated (a ["Dynamic Subdomain" of CERN DNS doc](https://service-dns.web.cern.ch/advanceddns.asp#lba));
* `REGISTER_IN_EXTERNAL_VIEW` - whether to update the `external` view thus making the domain resolvable externally.
(pass `true` to specify that the `external` view should be updated as well).

Apart from the arguments passed directly to the scripts, these environment variables must be available:

- `INTERNAL_TSIG_KEY` and `INTERNAL_TSIG_DATA` - credentials used to update the `internal` view;
- `EXTERNAL_TSIG_KEY` and `EXTERNAL_TSIG_DATA` - the same as above but these credentials are used to update the `external` view.
- `DRY_RUN` - if set to `true`, will not send any DNS updates to the DNS manager


## Integration tests

The CI automatically runs integration tests after building the new `publish-dns` container image.
The container image is installed into the [`publish-dns-integration-tests` project on PaaS Staging](https://paas-stg.cern.ch/k8s/cluster/projects/publish-dns-integration-tests).
The [associated Application Portal project](https://application-portal.web.cern.ch/manage/08d9d126-590e-4155-8250-cb6df7bd428d) has `openshift-admins` as the Administrators.

The Openshift project needs to be configured as follows:

```sh
# create project (if it doesn't already exist)
oc new-project publish-dns-integration-tests --description "Project to host CI integration tests of publish-dns component https://gitlab.cern.ch/paas-tools/okd4-deployment/ingress-publish-dns"

# inject RBAC rules (this needs to be done with an administrator account!)
oc apply -f <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: default-publish-dns-integration-tests
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: default-publish-dns-integration-tests
subjects:
- kind: ServiceAccount
  name: default-publish-dns-integration-tests
  namespace: publish-dns-integration-tests
roleRef:
  kind: ClusterRole
  apiGroup: rbac.authorization.k8s.io
  name: default-publish-dns # we assume that this already exists in the cluster
EOF

# create service account for CI
oc create serviceaccount publish-dns-ci
oc policy add-role-to-user edit -z publish-dns-ci
oc serviceaccounts get-token publish-dns-ci
```

Add the token printed by the last command in the [CI variables settings](https://gitlab.cern.ch/paas-tools/okd4-deployment/ingress-publish-dns/-/settings/ci_cd) as `OPENSHIFT_TOKEN` (check *Mask variable*).
