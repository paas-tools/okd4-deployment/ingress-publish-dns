FROM quay.io/openshift/origin-cli:4.12 as okd-cli

FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest
COPY --from=okd-cli /usr/bin/oc /usr/local/bin/

RUN yum install -y jq bind-utils && \
    yum clean all && rm -rf /var/cache/yum/

COPY ./publish_dns.sh /usr/local/bin/publish_dns
COPY ./liveness.sh /usr/local/bin/liveness
