#!/bin/bash

if [[ "$#" -ne 5 ]]; then
  echo "Not all arguments have been provided" 1>&2
  exit 1
fi

INGRESS_CONTROLLER=$1
DNS_MANAGER=$2
ZONE=$3
REGISTER_IN_EXTERNAL_VIEW=$4
PUBLISH_IPV6=$5

NSUPDATE_FILE="/tmp/ingress.nsupdate"
TTL=60

# don't re-run 'oc get --watch' immediately: while we're waiting for the creation of the endpoints resources by the Openshift-ingress-operator, this would hammer the API
while sleep 1; do
  RESOURCE_NAME=
  RESOURCE_TYPE=
  # We have several "types" of ingresses:
  # HostNetwork: regular Ingress where the IP address of the nodes is used directly
  # LoadBalancerService: Ingress that uses an OpenStack Loadbalancer service in front of it
  INGRESS_TYPE="$(oc get ingresscontroller "${INGRESS_CONTROLLER}" -n openshift-ingress-operator -o jsonpath='{.spec.endpointPublishingStrategy.type}')"
  if [[ "$INGRESS_TYPE" == "HostNetwork" ]]; then
    RESOURCE_TYPE="endpoints"
    RESOURCE_NAME="router-internal-${INGRESS_CONTROLLER}"
  elif [[ "$INGRESS_TYPE" == "LoadBalancerService" ]]; then
    RESOURCE_TYPE="services"
    RESOURCE_NAME="router-${INGRESS_CONTROLLER}"
  else
    echo "ERROR! Unsupported ingress type '${INGRESS_TYPE}'" 1>&2
    exit 1
  fi

  # Wait until the Kubernetes resource changes
  oc get "${RESOURCE_TYPE}" "${RESOURCE_NAME}" -o name -n openshift-ingress --watch=true | while read -r RESOURCE; do
    # $RESOURCE has the format "resource-type/resource-name", e.g. service/router-internal-default
    IPV4_ADDRESSES=
    IPV6_ADDRESSES=

    # Get the hostname for which we want to publish DNS entries in the end
    DNS_HOSTNAME="$(oc get ingresscontroller "${INGRESS_CONTROLLER}" -n openshift-ingress-operator -o jsonpath='{.status.domain}')"

    # Depending on the type of ingress, we need to retrieve the IPs from different status fields
    if [[ "$INGRESS_TYPE" == "HostNetwork" ]]; then
      IPV4_ADDRESSES="$(oc get "${RESOURCE}" -n openshift-ingress -o jsonpath='{.subsets[*].addresses[*].ip}')"
    elif [[ "$INGRESS_TYPE" == "LoadBalancerService" ]]; then
      # LoadBalancer services can either publish their hostname or their IP address in service.status
      LB_HOSTNAME="$(oc get "${RESOURCE}" -n openshift-ingress -o jsonpath='{.status.loadBalancer.ingress[0].hostname}')"
      if [[ -n "$LB_HOSTNAME" ]]; then
          # Note: OCCM publishes the LB hostname as "IP-ADDRESS.nip.io"
          # We need to resolve this hostname to an IP address - DO NOT use the $DNS_MANAGER for this, it's not responsible for the domain name!
          IPV4_ADDRESSES="$(dig +short "${LB_HOSTNAME}")"
       else
          IPV4_ADDRESSES="$(oc get "${RESOURCE}" -n openshift-ingress -o jsonpath='{.status.loadBalancer.ingress[0].ip}')"
      fi
    fi

    if [ -z "$IPV4_ADDRESSES" ]; then
      echo "No IP addresses found for ${RESOURCE}, skipping update."
      continue
    fi

    # Sometimes we might want to disable publishing AAAA records for IPv6 addresses of our nodes due to connectivity issues
    if [ "$PUBLISH_IPV6" = "true" ]; then
        # first do a reverse lookup of the IPv4 addresses, then do an AAAA lookup for the IPv6 addresses
        REVERSE_DNS_HOSTNAMES=$(for i in ${IPV4_ADDRESSES}; do dig @"${DNS_MANAGER}" +short -x "$i"; done)
        IPV6_ADDRESSES=$(for i in ${REVERSE_DNS_HOSTNAMES}; do dig @"${DNS_MANAGER}" +short "$i" AAAA; done)
    fi

    cat <<EOF > ${NSUPDATE_FILE}
server ${DNS_MANAGER}
zone ${ZONE}
update delete ${DNS_HOSTNAME} A
update delete ${DNS_HOSTNAME} AAAA
EOF

    for ipv4 in ${IPV4_ADDRESSES}; do
      echo "update add ${DNS_HOSTNAME} ${TTL} A ${ipv4}" >> ${NSUPDATE_FILE}
    done

    for ipv6 in ${IPV6_ADDRESSES}; do
      if [[ -n $ipv6 ]]; then
        echo "update add ${DNS_HOSTNAME} ${TTL} AAAA ${ipv6}" >> ${NSUPDATE_FILE}
      fi
    done

    # Ensure we also have a record *.<DNS_HOSTNAME> - we simply make it a CNAME <DNS_HOSTNAME>.
    # This is expected by OKD ingress, even though we won't really use it in CERN OKD4 deployments because
    # user apps use shared subdomains like app.cern.ch or web.cern.ch instead.
    # The one scenario where this record is useful is because the `routerCanonicalHostname` in route status
    # looks like `<something>.<router domain>` since https://bugzilla.redhat.com/show_bug.cgi?id=1901648
    # The `routerCanonicalHostname` needs to be functional since it tells users whose apps use manually-managed
    # external domains (such as `myapp.io`) where to point their external DNS records to.
    echo "update add *.${DNS_HOSTNAME} ${TTL} CNAME ${DNS_HOSTNAME}" >> ${NSUPDATE_FILE}

    echo "send" >> ${NSUPDATE_FILE}
    echo "quit" >> ${NSUPDATE_FILE}

    echo "Updating the internal view with the following changes:"
    echo
    cat ${NSUPDATE_FILE}
    echo

    # only publish DNS updates when we are not in dry-run mode
    if [[ "$DRY_RUN" != "true" ]]; then
        # update internal view
        nsupdate -y "hmac-sha512:${INTERNAL_TSIG_KEY}:${INTERNAL_TSIG_DATA}" "${NSUPDATE_FILE}"
        if [[ "$?" -ne 0 ]]; then
            break
        fi

        # update external view only if necessary
        if [[ ${REGISTER_IN_EXTERNAL_VIEW} == "true" ]]; then
            echo "Updating the external view with the same changes"
            nsupdate -y "hmac-sha512:${EXTERNAL_TSIG_KEY}:${EXTERNAL_TSIG_DATA}" "${NSUPDATE_FILE}"
            if [[ "$?" -ne 0 ]]; then
                break
            fi
        fi
    fi
    rm -f "${NSUPDATE_FILE}"
  done

  # clean up all dangling `oc` processes
  pkill -15 -f "oc get ${RESOURCE_NAME} .* --watch=true"
  # e.g. the file might still exist if there was an error while calling nsupdate
  rm -f "${NSUPDATE_FILE}"
done
